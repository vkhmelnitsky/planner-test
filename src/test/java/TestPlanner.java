import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.selenide.AllureSelenide;
import log4j.testlogger;
import log4j.utils.JsEgor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

public class TestPlanner {
    final static testlogger logger = new testlogger(TestPlanner.class);

    @BeforeAll
    public static void SetUP() {
        Configuration.startMaximized = true;
        Config.screenshots = true;
        WebDriverManager.chromedriver().setup();
        open(Config.baseUrl);
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(false));
        logger.debug("ChromeBrowset open to " + Config.baseUrl);
    }

    @Test
    @DisplayName("Step01: Успещная Авторизация в системе")
    public void step01() {
        $(By.id("id_username")).setValue(Config.login1);
        $(By.id("id_password")).setValue(Config.password);
        $(By.xpath("/html/body/div/div[2]/form/div[3]/button")).click();
    }
    @Test
    @DisplayName("Step02: Вход в профиль сотрудника")
    public void step02() {
        $(By.xpath("/html/body/div[1]/header/div[2]/div[2]/a/span[2]")).shouldHave(text("Elias Koch"));
        $(By.xpath("/html/body/div[1]/header/div[2]/div[2]/a/span[2]")).click();
        $(By.xpath("/html/body/div[1]/header/div[2]/div[2]/ul/li[1]/a")).shouldHave(text("Profile"));
        $(By.xpath("/html/body/div[1]/header/div[2]/div[2]/ul/li[1]/a")).click();
    }

    @Test
    @DisplayName("Step03: Проверка эллементов профиля")
    public void step03() {
        $$(By.xpath("//*[@id=\"content\"]/div[1]/ul")).shouldHave(CollectionCondition.texts("Profile"));
        $$(By.xpath("//*[@id=\"content\"]/div[1]/ul")).shouldHave(CollectionCondition.texts("Skills"));
        $$(By.xpath("//*[@id=\"content\"]/div[1]/ul")).shouldHave(CollectionCondition.texts("Goals"));
        $$(By.xpath("//*[@id=\"content\"]/div[1]/ul")).shouldHave(CollectionCondition.texts("TimeOff"));
        $$(By.xpath("//*[@id=\"content\"]/div[1]/ul")).shouldHave(CollectionCondition.texts("Projects"));
        $(By.xpath("//*[@id=\"content\"]/div[1]/ul/li[1]")).click();
        $$(By.xpath("//*[@id=\"skills-tabs\"]")).shouldHave(CollectionCondition.texts("Org Chart"));
        $$(By.xpath("//*[@id=\"skills-tabs\"]")).shouldHave(CollectionCondition.texts("Schedule"));
        $$(By.xpath("//*[@id=\"skills-tabs\"]")).shouldHave(CollectionCondition.texts("Job Titles"));
        $$(By.xpath("//*[@id=\"skills-tabs\"]")).shouldHave(CollectionCondition.texts("Comments"));
        $$(By.xpath("//*[@id=\"skills-tabs\"]")).shouldHave(CollectionCondition.texts("Other Info"));
    }

    @Test
    @DisplayName("Step04: Проверка скрытия дерева в Org Chart")
    public void step04() {
        $(By.xpath("//*[@id=\"tree\"]/div/div/div[3]/div[1]")).click();
        $(By.xpath("//*[@id=\"tree\"]/div/div/div[2]")).shouldNotHave(visible);
    }

    @Test
    @DisplayName("Step05: Проверка содержимого Schedule")
    public void step05() {
        $(By.xpath("//*[@id=\"timeNormTab\"]")).click();
        if (Config.IsElementVisibleid("timeNorm") == true) {
            $$("#timeNorm > div > table > thead > tr > th").shouldHave(CollectionCondition.size(3));
            $$("#timeNorm > div > table > thead > tr > th").shouldHave(CollectionCondition.texts("Period start", "Period end", "Weekly hour norm"));
        }
    }

    @Test
    @DisplayName("Step06: Проверка заголовков таблицы в Job Titles")
    public void step06() {
        $(By.id("jobTitlesTab")).click();
        if (Config.IsElementVisibleCssSelector("#content > div.main > div > div.profile-content > div.box > div.box-content.tab-content.corporate-profile-tab > div > div.employee-titles.js-rg-employee-titles")) {
            $$("#content > div.main > div > div.profile-content > div.box > div.box-content.tab-content.corporate-profile-tab > div > div.employee-titles.js-rg-employee-titles > div > table > thead > tr > th").shouldHave(CollectionCondition.size(8));
            $$("#content > div.main > div > div.profile-content > div.box > div.box-content.tab-content.corporate-profile-tab > div > div.employee-titles.js-rg-employee-titles > div > table > thead > tr > th").shouldHave(CollectionCondition.texts("DEPARTMENT", "GROUP", "TITLE", "LOCATION", "ACTION", "START AT", "COMMENT", " "));
        }
    }

    //    @Test
//    @DisplayName("Step07: Добавление Job Titles и проверка, что он появился в таблице")
//    public void step07(){
//        open("https://planner-sandbox.dev.thumbtack.net/employees/225/corporate/job_titles/");
//            $(By.id("id_username")).setValue(Config.login1);
//            $(By.id("id_password")).setValue(Config.password);
//            $(By.xpath("/html/body/div/div[2]/form/div[3]/button")).click();
//        open("https://planner-sandbox.dev.thumbtack.net/employees/225/corporate/job_titles/");
//        $(By.xpath("//*[@id=\"content\"]/div[2]/div/div[2]/div[2]/div[2]/div/button")).click();
//        $(By.cssSelector("#content > div.main > div > div.profile-content > div.box > div.box-content.tab-content.corporate-profile-tab > div > div.add-job-title.js-rg-add-job-title > div > form > div > div"))
//                .shouldHave(visible);
//        $(By.cssSelector("#content > div.main > div > div.profile-content > div.box > div.box-content.tab-content.corporate-profile-tab > div > div.add-job-title.js-rg-add-job-title > div > form > div > div > div.modal-body > select.form-control.department-select.js-department"))
//                .selectOptionContainingText("Software Development");
//        $(By.cssSelector("#content > div.main > div > div.profile-content > div.box > div.box-content.tab-content.corporate-profile-tab > div > div.add-job-title.js-rg-add-job-title > div > form > div > div > div.modal-body > select.form-control.group-select.js-group"))
//                .selectOptionContainingText("Web");
//        $(By.xpath("//*[@id=\"content\"]/div[2]/div/div[2]/div[2]/div[2]/div/div[1]/div/form/div/div/div[2]/select[3]"))
//                .selectOptionContainingText("Senior Software Engineer of 1st category");
//        $(By.xpath("//*[@id=\"content\"]/div[2]/div/div[2]/div[2]/div[2]/div/div[1]/div/form/div/div/div[2]/select[5]"))
//                .selectOptionContainingText("Saratov");
//        $(By.cssSelector("#content > div.main > div > div.profile-content > div.box > div.box-content.tab-content.corporate-profile-tab > div > div.add-job-title.js-rg-add-job-title > div > form > div > div > div.modal-body > input"))
//                .setValue(Config.GetCurrentDateforField());
//        $(By.xpath("//*[@id=\"content\"]/div[2]/div/div[2]/div[2]/div[2]/div/div[1]/div/form/div/div/div[2]/textarea"))
//                .setValue("My comment");
//        $(By.cssSelector("#content > div.main > div > div.profile-content > div.box > div.box-content.tab-content.corporate-profile-tab > div > div.add-job-title.js-rg-add-job-title > div > form > div > div > div.modal-footer > button.btn.btn-primary.js-save-job-title-btn"))
//                .click();
//        $(By.xpath("//*[@id=\"content\"]/div[2]/div/div[2]/div[2]/div[2]/div/div[2]/div/table/tbody/tr[1]/td[1]")).shouldHave(Condition.text("Software Development"));
//        $(By.xpath("//*[@id=\"content\"]/div[2]/div/div[2]/div[2]/div[2]/div/div[2]/div/table/tbody/tr[1]/td[2]")).shouldHave(Condition.text("Web"));
//        $(By.xpath("//*[@id=\"content\"]/div[2]/div/div[2]/div[2]/div[2]/div/div[2]/div/table/tbody/tr[1]/td[3]")).shouldHave(Condition.text("Senior Software Engineer of 1st category "));
//        $(By.xpath("//*[@id=\"content\"]/div[2]/div/div[2]/div[2]/div[2]/div/div[2]/div/table/tbody/tr[1]/td[4]")).shouldHave(Condition.text("Saratov"));
//        $(By.xpath("//*[@id=\"content\"]/div[2]/div/div[2]/div[2]/div[2]/div/div[2]/div/table/tbody/tr[1]/td[5]")).shouldHave(Condition.text("Promoted"));
//        $(By.xpath("//*[@id=\"content\"]/div[2]/div/div[2]/div[2]/div[2]/div/div[2]/div/table/tbody/tr[1]/td[6]")).shouldHave(Condition.text("2018-12-28"));
//        $(By.xpath("//*[@id=\"content\"]/div[2]/div/div[2]/div[2]/div[2]/div/div[2]/div/table/tbody/tr[1]/td[7]")).shouldHave(Condition.text("My comment"));
//    }
    @Test
    @DisplayName("Step08: Переход в подменю Comments и создание комментария")
    public void step08() {
        $(By.id("commentsTab")).click();
        $(By.cssSelector("#content > div.main > div > div.profile-content > div.box > div.box-content.tab-content.corporate-profile-tab > div:nth-child(1) > form > div > textarea")).shouldHave(visible);
        $(By.cssSelector("#content > div.main > div > div.profile-content > div.box > div.box-content.tab-content.corporate-profile-tab > div:nth-child(1) > form > div > textarea")).click();
        $(By.cssSelector("#content > div.main > div > div.profile-content > div.box > div.box-content.tab-content.corporate-profile-tab > div:nth-child(1) > form > div > textarea")).sendKeys("AutotestFirstComment");
        $(By.cssSelector("#content > div.main > div > div.profile-content > div.box > div.box-content.tab-content.corporate-profile-tab > div:nth-child(1) > form > button")).click();
    }

    @Test
    @DisplayName("Step09: Проверка созданного ранее комментария и удаление его")
    public void step09() {
        $(By.cssSelector("#content > div.main > div > div.profile-content > div.box > div.box-content.tab-content.corporate-profile-tab > div.comments > div > div.panel-heading > span")).shouldHave(text("Elias Koch"));
        $(By.cssSelector("#content > div.main > div > div.profile-content > div.box > div.box-content.tab-content.corporate-profile-tab > div.comments > div > div.panel-body > span.text")).shouldHave(text("AutotestFirstComment"));
        $(By.cssSelector("#content > div.main > div > div.profile-content > div.box > div.box-content.tab-content.corporate-profile-tab > div.comments > div > div.panel-body > span.date")).shouldHave(text(Config.Date("GetCurrentDateUS")));
        $(By.cssSelector("#content > div.main > div > div.profile-content > div.box > div.box-content.tab-content.corporate-profile-tab > div.comments > div > div.panel-heading > button")).click();
        $(By.cssSelector("#content > div.main > div > div.profile-content > div.box > div.box-content.tab-content.corporate-profile-tab > div.comments > div > div.panel-body > span.text")).shouldNotHave(text("AutotestFrstComment"));
    }
    @Test
    @DisplayName("Step10: Проверка Profile details")
    public void step10() {
        $(By.cssSelector("#content > div.main > div > div.profile-details > div.profile-details__section > h2")).shouldHave(text("Elias Koch"));
        $(By.cssSelector("#content > div.main > div > div.profile-details > div.profile-details__section > div.profile__location")).shouldHave(or("Location", text("Omsk"), text("Saratov")));
        $(By.cssSelector("#content > div.main > div > div.profile-details > div.profile-details__section > div.profile__actions > a")).shouldHave(text("Edit profile"));
        $(By.cssSelector("#content > div.main > div > div.profile-details > div.profile-details__section > div.profile__actions > button")).shouldHave(text("Dismiss"));
        $(By.cssSelector("#profile-more > div.profile-details__section.user-details-info > h3")).shouldHave(text("CONTACT"));
        $(By.cssSelector("#profile-more > div:nth-child(2) > h3")).shouldHave(text("PERSONAL"));
        $(By.cssSelector("#profile-more > div:nth-child(3) > div > div.achievements__header > h3")).shouldHave(text("ACHIEVEMENTS"));
    }

    @Test
    @DisplayName("Step11: Изменение Edit Profile")
    public void step11() {
        $(By.cssSelector("#content > div.main > div > div.profile-details > div.profile-details__section > div.profile__actions > a")).click();
        $(By.cssSelector("#id_date_of_birth")).hover();
        $(By.cssSelector("#id_date_of_birth")).clear();
        $(By.cssSelector("#id_date_of_birth")).sendKeys(Config.Date("DateOfBirth"));
        $(By.id("id_email_personal")).setValue(Config.email);
        $(By.id("id_phone_number")).setValue(Config.phone);
        $(By.id("id_skype")).setValue(Config.skype);
        $(By.cssSelector("#commonSettings > form > button")).click();
        $(By.cssSelector("#notify span")).shouldHave(Condition.text("Saved"));
    }

    @Test
    @DisplayName("Step12: Данные в Profile details изменились, после сохранения новой информации")
    public void step12() {
        //Contact
        $(By.xpath("/html/body/div[1]/header/div[2]/div[2]/a/span[2]")).shouldHave(text("Elias Koch"));
        $(By.xpath("/html/body/div[1]/header/div[2]/div[2]/a/span[2]")).click();
        $(By.xpath("/html/body/div[1]/header/div[2]/div[2]/ul/li[1]/a")).shouldHave(text("Profile"));
        $(By.xpath("/html/body/div[1]/header/div[2]/div[2]/ul/li[1]/a")).click();
        $(By.cssSelector("#profile-more > div.profile-details__section.user-details-info > div > p:nth-child(1)")).shouldHave(Condition.text(Config.phone));
        $(By.cssSelector("#profile-more > div.profile-details__section.user-details-info > div > p:nth-child(3)")).shouldHave(Condition.text(Config.email));
        $(By.cssSelector("#profile-more > div.profile-details__section.user-details-info > div > p:nth-child(4)")).shouldHave(Condition.text(Config.skype));
        $(By.cssSelector("#profile-more > div.profile-details__section.user-details-info > div > p:nth-child(4)")).parent().parent().shouldHave(text("Contact"));
        //Personal
        $(By.cssSelector("#profile-more > div:nth-child(2) > div > p")).shouldHave(text(Config.Date("DateOfBirthCheck")));
        $(By.cssSelector("#profile-more > div:nth-child(2) > div > p")).parent().parent().shouldHave(text("Personal"));
        int a = 5;
    }

    @Test
    @DisplayName("Step13: Создание Achievements без типа: и получение INTERNAL SERVER ERROR")
    public void step13() {
        $(By.cssSelector("#profile-more > div:nth-child(3) > div > div.achievements__container > div > div > div")).click();
        $(By.id("add_achievement_window")).shouldHave(visible);
        $(By.cssSelector("#add_achievement_window > div > div > form > div.modal-footer > div > button.add_achievement_window__send_button.btn.btn-primary")).click();
        $(By.id("add_achievement_window")).shouldHave(visible);
        $(By.cssSelector("#add_achievement_window > div > div > form > div.modal-body > div.row.form-group.required > div > textarea")).setValue("top comment");
        $(By.cssSelector("#add_achievement_window > div > div > form > div.modal-footer > div > button.add_achievement_window__send_button.btn.btn-primary")).click();
        $(By.className("noty_text")).shouldHave(text("INTERNAL SERVER ERROR"));
    }

    @Test
    @DisplayName("Step14: Создание Achievements с типом  Thsnks")
    public void step14() {
        $(By.cssSelector("#profile-more > div:nth-child(3) > div > div.achievements__container > div > div > div")).click();
        $(By.id("add_achievement_window")).shouldHave(visible);
        $(By.cssSelector("#add_achievement_window > div > div > form > div.modal-footer > div > button.add_achievement_window__send_button.btn.btn-primary")).click();
        $(By.id("add_achievement_window")).shouldHave(visible);
        $(By.cssSelector("#add_achievement_window > div > div > form > div.modal-body > div.row.form-group.required > div > textarea")).setValue("top comment");
        $(By.cssSelector("#add_achievement_window > div > div > form > div.modal-body > div:nth-child(3) > div > div > div:nth-child(2) > img")).click();
        $(By.cssSelector("#add_achievement_window > div > div > form > div.modal-footer > div > button.add_achievement_window__send_button.btn.btn-primary")).click();
    }

    @Test
    @DisplayName("Step15: Проверка ранее созданного Achievements с типом  Thanks и удаление его")
    public void step15() {
        $(By.cssSelector("#profile-more > div:nth-child(3) > div > div.achievements__container > div.achievement.js-achievement-show-details > div")).shouldHave(text("1"));
        $(By.cssSelector("#profile-more > div:nth-child(3) > div > div.achievements__container > div.achievement.js-achievement-show-details > img")).shouldHave(visible);
        $(By.cssSelector("#profile-more > div:nth-child(3) > div > div.achievements__container > div.achievement.js-achievement-show-details > img")).click();
        $(By.cssSelector("#employee_achievements_window > div > div > div.employee_achievements_window__list.modal-body > table > thead > tr > th:nth-child(1)")).shouldHave(text("Sender"));
        $(By.cssSelector("#employee_achievements_window > div > div > div.employee_achievements_window__list.modal-body > table > thead > tr > th:nth-child(2)")).shouldHave(text("Date"));
        $(By.cssSelector("#employee_achievements_window > div > div > div.employee_achievements_window__list.modal-body > table > thead > tr > th:nth-child(3)")).shouldHave(text("Comment"));
        $(By.cssSelector("#employee_achievements_window > div > div > div.employee_achievements_window__list.modal-body > table > tbody > tr > td:nth-child(1)")).shouldHave(text("Elias Koch"));
        $(By.cssSelector("#employee_achievements_window > div > div > div.employee_achievements_window__list.modal-body > table > tbody > tr > td:nth-child(2)")).shouldHave(text(Config.Date("GetCurrentDateForAchievements")));
        $(By.cssSelector("#employee_achievements_window > div > div > div.employee_achievements_window__list.modal-body > table > tbody > tr > td:nth-child(3)")).shouldHave(text("top comment"));
        $(By.cssSelector("#employee_achievements_window > div > div > div.employee_achievements_window__list.modal-body > table > tbody > tr > td.employee_achievements_window__remove_section.js-achievements-actions > span")).click();
        $(By.cssSelector("#employee_achievements_window > div > div > div.employee_achievements_window__list.modal-body > table > tbody > tr > td.employee_achievements_window__remove_section.js-achievements-actions > span:nth-child(2) > a.js-achievements-remove-cancel"))
                .shouldHave(text("Cancel"));
        $(By.cssSelector("#employee_achievements_window > div > div > div.employee_achievements_window__list.modal-body > table > tbody > tr > td.employee_achievements_window__remove_section.js-achievements-actions > span:nth-child(2) > a.js-achievements-remove-confirm"))
                .shouldHave(text("Confirm"));
        $(By.cssSelector("#employee_achievements_window > div > div > div.employee_achievements_window__list.modal-body > table > tbody > tr > td.employee_achievements_window__remove_section.js-achievements-actions > span:nth-child(2) > a.js-achievements-remove-confirm"))
                .click();
        $(By.cssSelector("#employee_achievements_window > div > div > div.employee_achievements_window__list.modal-body > table > tbody")).shouldHave(empty);
    }
    @AfterEach
    public void js(){
        JsEgor.jsExeptions();
    }
}
