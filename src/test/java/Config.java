import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.openqa.selenium.By;

import java.util.Locale;
import java.util.logging.Logger;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;


public abstract class Config {
    private static Logger LOG = Logger.getLogger(com.codeborne.selenide.Configuration.class.getName());
    public static String baseUrl = ("https://planner-sandbox.dev.thumbtack.net");
    public static String login1;
    public static String password;
    public static String email;
    public static String skype;
    public static String phone;
    @Deprecated
    public static String chromeSwitches = System.getProperty("selenide.chrome.switches", System.getProperty("chrome.switches"));
    public static boolean driverManagerEnabled;
    public static boolean headless;
    public static String labelname;
    public static boolean screenshots = Boolean.parseBoolean(System.getProperty("Configuration.screenshots", "false"));
    public static String browserBinary;
    public Config() {
    }
    static {
        LOG = Logger.getLogger(com.codeborne.selenide.Configuration.class.getName());
        driverManagerEnabled = Boolean.parseBoolean(System.getProperty("selenide.driverManagerEnabled", "true"));
        headless = Boolean.parseBoolean(System.getProperty("selenide.headless", "false"));
        browserBinary = System.getProperty("selenide.browserBinary", "");
        login1 = ("kochelias");
        password = ("123456");
        email = ("kochelias@example.com");
        skype = ("skypexamle");
        phone = (PhoneGenerator.Phonegen());
    }
    public static boolean IsElementVisibleCssSelector(String cssSelector) {
        try {
            $(By.cssSelector(cssSelector)).shouldHave(visible);
            return true;
        } catch (org.openqa.selenium.NoSuchElementException e) {
            return false;
        }
    }
    public static boolean IsElementVisibleid(String guid) {
        try {
            $(By.id(guid)).shouldHave(visible);
            return true;
        } catch (org.openqa.selenium.NoSuchElementException e) {
            return false;
        }
    }
    public static String Date(String format){
        String date = "";
        if (format == "GetCurrentDateUS"){
            DateTimeZone zone = DateTimeZone.forID("America/Edmonton");
            DateTime dateForParse = DateTime.now().withZone(zone);
            DateTimeFormatter dtfOut = DateTimeFormat.forPattern("MMM. d, YYYY").withLocale(Locale.ENGLISH);
            date = dtfOut.print(dateForParse);
        }
        if (format == "GetCurrentDateforField"){
            DateTime dateForParse = DateTime.now();
            DateTimeFormatter dtfOut = DateTimeFormat.forPattern("YYYY-MM-DD");
            date = dtfOut.print(dateForParse);
        }
        if (format == "GetCurrentDateForAchievements") {
            DateTimeZone zone = DateTimeZone.forID("America/Edmonton");
            DateTime dateForParse = DateTime.now().withZone(zone);
            DateTimeFormatter dtfOut = DateTimeFormat.forPattern("dd/MM/YYYY").withLocale(Locale.ENGLISH);
            date = dtfOut.print(dateForParse);
        }
        if (format == "DateOfBirth"){
            DateTime dateForParse = DateTime.now().plusMonths(1).minusYears(21);
            DateTimeFormatter dtfOut = DateTimeFormat.forPattern("YYYY-MM-dd").withLocale(Locale.ENGLISH);
            date = dtfOut.print(dateForParse);
        }
        if (format == "DateOfBirthCheck"){
            DateTime dateForParse = DateTime.now().plusMonths(1);
            DateTimeFormatter dtfOut = DateTimeFormat.forPattern("MMMMM d").withLocale(Locale.ENGLISH);
            date = dtfOut.print(dateForParse);
        }
        return  date;
    }
}
